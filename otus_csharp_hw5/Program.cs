﻿
using otus_csharp_hw5;
using System.Diagnostics;

F f = new F() { i1 = 1, i2 = 2, i3 = 3, i4 = 4, i5 = 5 };
var stopwatch = new Stopwatch();
int countIteration = 10000;

Console.WriteLine("Среда разработки: Microsoft Visual Studio Community 2022 (64-разрядная версия): Версия 17.1.3");

Console.WriteLine($"Сериализация объекта в CSV. Количество итераций {countIteration}");
stopwatch.Start();
for (int i = 0; i < countIteration; i++) { SerializerCSV.Serialize(f); }
stopwatch.Stop();
string decription = SerializerCSV.Serialize(f);
Console.WriteLine(decription);
Console.WriteLine($"Время на сериализацию CSV: {stopwatch.ElapsedMilliseconds} ms.");

Console.WriteLine($"Десериализация CSV в объект. Количество итераций {countIteration}");
stopwatch.Start();
for (int i = 0; i < countIteration; i++) { SerializerCSV.Deserialize<F>(decription); }
stopwatch.Stop();
F f_copy = SerializerCSV.Deserialize<F>(decription);
Console.WriteLine($"Время на десериализацию CSV: {stopwatch.ElapsedMilliseconds} ms.");

Console.WriteLine("Контрольная сериализация десериализованного объекта в CSV");
decription = SerializerCSV.Serialize(f_copy);
Console.WriteLine(decription);

Console.WriteLine($"Сериализация объекта в JSON с помощью System.Text.Json. Количество итераций {countIteration}");
stopwatch.Start();
for (int i = 0; i < countIteration; i++) { SerializerJSON.Serialize(f); }
stopwatch.Stop();
Console.WriteLine($"Время на сериализацию JSON: {stopwatch.ElapsedMilliseconds} ms.");
decription = SerializerJSON.Serialize(f);
Console.WriteLine(decription);

Console.WriteLine($"Десериализация JSON в объект с помощью System.Text.Json. Количество итераций {countIteration}");
stopwatch.Start();
for (int i = 0; i < countIteration; i++) { SerializerJSON.Deserialize<F>(decription); }
stopwatch.Stop();
Console.WriteLine($"Время на десериализацию JSON: {stopwatch.ElapsedMilliseconds} ms.");
f_copy  = SerializerJSON.Deserialize<F>(decription);

Console.WriteLine("Контрольная сериализация десериализованного объекта в JSON");
decription = SerializerJSON.Serialize(f_copy);
Console.WriteLine(decription);

Console.ReadLine();