﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace otus_csharp_hw5
{
    public static class SerializerCSV
    {

        private static readonly string Separator = ",";

        public static string Serialize<T>(T obj)
        {

            FieldInfo[] fields          = typeof(T).GetFields();
            PropertyInfo[] properties   = typeof(T).GetProperties();

            var description = new StringBuilder();
            var header      = string.Join(Separator, fields.Select(f => f.Name)
                .Concat(properties.Select(p => p.Name)));

            description.AppendLine(header);

            var data = string.Join(Separator, fields.Select(f => f.GetValue(obj) ?? "")
                .Concat(properties.Select(p => p.GetValue(obj) ?? "")));

            description.AppendLine(data);

            return description.ToString();

        }

        public static T Deserialize<T>(string description)
        {

            T obj = Activator.CreateInstance<T>();

            var lines = description.Split('\r');
            if (lines.Length == 0)
            {
                return obj;
            }

            var header  = lines[0].Split(Separator);
            var data    = lines[1].Split(Separator);

            FieldInfo[] fields          = typeof(T).GetFields();
            PropertyInfo[] properties   = typeof(T).GetProperties();

            for (int i = 0; i < header.Length; i++)
            {
                var field = fields.FirstOrDefault(x => x.Name == header[i]);
                if (field != null)
                {
                    Type fieldType = field.FieldType;
                    field.SetValue(obj, Convert.ChangeType(data[i], fieldType));
                }

                var prop = properties.FirstOrDefault(x => x.Name == header[i]);
                if (prop != null)
                {
                    prop.SetValue(obj, data[i]);
                }
            }
            return obj;
        }
    }
}
