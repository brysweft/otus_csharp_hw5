﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Text.Json.Nodes;
using System.Threading.Tasks;

namespace otus_csharp_hw5
{
    public static class SerializerJSON
    {

        private static readonly JsonSerializerOptions Options = new JsonSerializerOptions { IncludeFields = true };

        public static string Serialize<T>(T obj) {

            return JsonSerializer.Serialize(obj, Options);

        }

        public static T Deserialize<T>(string description)
        {
            return JsonSerializer.Deserialize<T>(description, Options);
        }
    }
}
